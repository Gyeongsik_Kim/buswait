package com.gyungdal.buswait;

/**
 * Created by GyungDal on 2016-09-23.
 */

public class Config {
    public static class BUS_TYPES{
        public static final int TRUNK = 101; //간선
        public static final int BRANCH = 102; //지선
        public static final int GREATER = 103; //광역
        public static final int LOOP = 104; //순환
        public static final int AIRPORT = 105; //공항
        public static final int NORMAL = 106; //일반
        public static final int SEAT = 107; //좌석
        public static final int QUICK = 108; //급행
        public static final int TOWN = 109; //마을
        public static final int DIRECT = 110; //직행
        public static final int SUBURB = 111; //외곽
        public static final int OUTER = 112; //시외
        public static final int RURAL = 113; //농어촌
    }

    public static final String DBNAME = "bus";
    public static final int DBVERSION = 1;
    public static final String BUS_STATION_PATH = "http://map.search.daum.net/mapsearch/search_bus.daum?type=busStop1&q=";
    public static final String BUS_STATION_PAGE = "&spage=";
    public static final String BUS_LIST_PATH = "http://map.daum.net/bus/stop.json?callback=&busstopid=";
    public static final String REFERER = "http://map.daum.net/";
    public static final String[] AVAILABLE_ZONE =
            {"서울시", "경기도", "인천시", "부산시", "대구시"
            , "대전시", "울산시", "청주시", "전주시", "여수시"
            , "순천시", "광주시", "포항시", "김해시", "창원시"
            , "양산시", "진주시", "경산시", "제주도", "세종시"
            , "천안시", "아산시", "춘천시", "원주시", "통영시"
            , "거제시", "광양시"};
}
