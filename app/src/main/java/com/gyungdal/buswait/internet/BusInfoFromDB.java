package com.gyungdal.buswait.internet;

import android.os.AsyncTask;
import android.util.Log;

import com.gyungdal.buswait.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by GyungDal on 2016-09-29.
 */

public class BusInfoFromDB extends AsyncTask<Void, Void, Item> {
    private static final String TAG = BusInfoFromDB.class.getName();
    private String station, id, data;

    public BusInfoFromDB(String station, String id){
        this.station = station;
        this.id = id;
    }

    @Override
    protected Item doInBackground(Void... params) {
        try {
            URL url = new URL(Config.BUS_LIST_PATH + station);
            Log.i(TAG, url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Referer", Config.REFERER);
            conn.setDoInput(true);

            switch (conn.getResponseCode()) {
                case 200:
                case 201: {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    while ((data = br.readLine()) != null)
                        sb.append(data + "\n");

                    data = sb.toString();
                    Log.i(TAG, data);
                    break;
                }
                default:
                    Log.e(TAG, "Request Fail");
                    return null;
            }
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("lines");
            Log.i(TAG, "" + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                Log.i(TAG, jsonArray.getJSONObject(i).toString());
                Item item = new Item();
                if (jsonArray.getJSONObject(i)
                        .getString("name").equals(id)) {
                    item.setCanRealTimeUpdate(true);
                    item.setName(jsonArray.getJSONObject(i).getString("name"));
                    item.setLocate(jsonArray.getJSONObject(i).getString("direction"));
                    item.setExtra(String.valueOf(jsonArray.getJSONObject(i)
                            .getJSONObject("arrival").getInt("arrivalTime")));
                    item.setIsStation(false);
                    return item;
                    }
                }
            } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }
}