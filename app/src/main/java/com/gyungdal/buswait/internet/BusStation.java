package com.gyungdal.buswait.internet;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.gyungdal.buswait.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by GyungDal on 2016-09-23.
 */

public class BusStation extends AsyncTask<Void, Integer, ArrayList<Item>> {
    private static final String TAG = BusStation.class.getName();
    private String data, search;
    private Context context;
    private ArrayList<Item> result;
    private int total;
    private ProgressDialog asyncDialog;
    public BusStation(Context context, String search){
        result = new ArrayList<>();
        this.context = context;
        this.search = search;
        total = 100;
    }
    @Override
    protected void onPreExecute() {
        asyncDialog = new ProgressDialog(context);
        asyncDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        asyncDialog.setMessage("작업 시작");
        asyncDialog.show();
        super.onPreExecute();
    }
    @Override
    protected void onProgressUpdate(Integer... progress) {
        asyncDialog.setMessage(progress[0] + "%");
        asyncDialog.setProgress(progress[0]);
    }

    @Override
    protected ArrayList<Item> doInBackground(Void... params) {
        try{
            asyncDialog.setMax(total);
            for(int page = 1; page <= total; page++) {
                publishProgress(total / page);
                URL url = new URL(Config.BUS_STATION_PATH + search + Config.BUS_STATION_PAGE + page);
                Log.i(TAG, url.toString());
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestProperty("Referer", Config.REFERER);
                conn.setDoInput(true);
                asyncDialog.setProgress(page);
                switch (conn.getResponseCode()) {
                    case 200:
                    case 201: {
                        BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        StringBuilder sb = new StringBuilder();
                        while ((data = br.readLine()) != null)
                            sb.append(data + "\n");
                        br.close();
                        data = sb.toString();
                        Log.i(TAG, data);
                        break;
                    }
                    default:
                        Log.e(TAG, "Request Fail");
                        return null;
                }
                JSONObject jsonObject = new JSONObject(data);
                Log.i(TAG, "" + (int) jsonObject.get("total_count"));
                JSONArray jsonArray = jsonObject.getJSONArray("List");
                if(total == 100 && page == 1) {
                    total = jsonObject.getInt("total_count") / jsonArray.length();
                    if(jsonObject.getInt("total_count") % jsonArray.length() != 0)
                        total++;
                    asyncDialog.setMax(total);
                }
                asyncDialog.setProgress(page);
                for (int i = 0; i < jsonArray.length(); i++) {
                    Log.i(TAG, jsonArray.getJSONObject(i).toString());
                    Item item = new Item();
                    item.setName(jsonArray.getJSONObject(i).getString("BUSSTOP_NAME"));
                    item.setLocate(jsonArray.getJSONObject(i).getString("direction"));
                    item.setId(jsonArray.getJSONObject(i).getString("DOCID"));
                    item.setExtra(jsonArray.getJSONObject(i).getString("STOP_BUS_TYPE"));
                    item.setIsStation(true);
                    item.setCanRealTimeUpdate(false);
                    result.add(item);
                }
            }
            Log.i(TAG, String.valueOf(result.size()));
            return result;
        }catch(Exception e){
            Log.e(TAG, e.getMessage());
        }
            return null;
    }

    @Override
    protected void onPostExecute(ArrayList<Item> result) {
        asyncDialog.dismiss();
        super.onPostExecute(result);
    }
}
