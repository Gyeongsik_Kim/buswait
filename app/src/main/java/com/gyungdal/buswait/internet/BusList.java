package com.gyungdal.buswait.internet;

import android.os.AsyncTask;
import android.util.Log;

import com.gyungdal.buswait.Config;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by GyungDal on 2016-09-23.
 */

public class BusList extends AsyncTask<Void, Void, ArrayList<Item>> {
    private static final String TAG = BusList.class.getName();
    private String station, data;
    private ArrayList<Item> result;

    public BusList(String station) {
        result = new ArrayList<>();
        this.station = station;
    }

    @Override
    protected ArrayList<Item> doInBackground(Void... params) {
        try {
            URL url = new URL(Config.BUS_LIST_PATH + station);
            Log.i(TAG, url.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Referer", Config.REFERER);
            conn.setDoInput(true);

            switch (conn.getResponseCode()) {
                case 200:
                case 201: {
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    while ((data = br.readLine()) != null)
                        sb.append(data + "\n");

                    data = sb.toString();
                    Log.i(TAG, data);
                    break;
                }
                default:
                    Log.e(TAG, "Request Fail");
                    return null;
            }
            JSONObject jsonObject = new JSONObject(data);
            JSONArray jsonArray = jsonObject.getJSONArray("lines");
            Log.i(TAG, "" + jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                Log.i(TAG, jsonArray.getJSONObject(i).toString());
                Item item = new Item();
                Log.i(TAG, jsonArray.getJSONObject(i).getString("realTime"));
                if (jsonArray.getJSONObject(i).getString("realTime").contains("tr")) {
                    item.setCanRealTimeUpdate(true);
                    item.setName(jsonArray.getJSONObject(i).getString("name"));
                    if (!jsonArray.getJSONObject(i).toString().contains("direction")) {
                        item.setLocate("Not found...");
                    } else {
                        item.setLocate(jsonArray.getJSONObject(i).getString("direction"));
                    }
                    item.setExtra(String.valueOf(jsonArray.getJSONObject(i)
                            .getJSONObject("arrival").getInt("arrivalTime")));
                    item.setIsStation(false);
                } else {
                    item.setCanRealTimeUpdate(false);
                    String name = jsonArray.getJSONObject(i).getString("name");
                    item.setName(name.substring(0, name.indexOf('(') - 1));
                    item.setLocate(name.substring(name.indexOf('(') - 1));
                    item.setExtra("1");
                    item.setId(item.getId());
                }
                if (!item.getExtra().contains("0"))
                    item.setExtra("자료 구해오기 실패...");
                result.add(item);
                //기다려야되는 시간이 0인 경우에는 주로 가짜가 많아서 제거
            }
            Log.i(TAG, "" + result.size());
            return result;
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
        return null;
    }
}
