package com.gyungdal.buswait.internet;

/**
 * Created by GyungDal on 2016-09-23.
 */

public class Item {
    private boolean isStation, canRealTimeUpdate, isSetUp;
    private String name; //~~ 정류장, (버스번호)
    private String locate; //~방면
    private String extra; //위치 또는 ~분후 도착
    private String id;

    public Item(String name, String locate, String extra, String id){
        this.name = name;
        this.locate = locate;
        this.extra = extra;
        this.id = id;
        this.isStation = true;
        this.canRealTimeUpdate = this.isSetUp = false;
    }

    public Item(String name, String locate, String extra
            , String id, boolean isStation, boolean canRealTimeUpdate){
        this.name = name;
        this.locate = locate;
        this.extra = extra;
        this.id = id;
        this.isStation = isStation;
        this.canRealTimeUpdate = canRealTimeUpdate;
    }

    public Item(){
        this.name = this.locate = this.extra = this.id = "";
    }

    public void setIsSetUp(boolean isSetUp){
        this.isSetUp = isSetUp;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setLocate(String locate){
        this.locate = locate;
    }

    public void setIsStation(boolean isStation){
        this.isStation = isStation;
    }

    public void setCanRealTimeUpdate(boolean canRealTimeUpdate){
        this.canRealTimeUpdate = canRealTimeUpdate;
    }

    public void setExtra(String extra){
        this.extra = extra;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getName(){
        return this.name;
    }

    public String getLocate(){
        return this.locate;
    }

    public String getExtra(){
        return this.extra;
    }

    public boolean getCanRealTimeUpdate(){
        return this.canRealTimeUpdate;
    }

    public boolean getIsSetUp(){
        return this.isSetUp;
    }
    public String getId(){
        return this.id;
    }

    public boolean getIsStation(){
        return this.isStation;
    }
}
