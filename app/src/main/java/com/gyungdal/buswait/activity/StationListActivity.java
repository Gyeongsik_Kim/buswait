package com.gyungdal.buswait.activity;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.gyungdal.buswait.R;
import com.gyungdal.buswait.activity.Recyclerview.Adapter;
import com.gyungdal.buswait.internet.BusStation;

import java.util.concurrent.ExecutionException;


/**
 * Created by GyungDal on 2016-09-26.
 */

public class StationListActivity extends AppCompatActivity {
    private static final String TAG = StationListActivity.class.getName();
    private EditText stationName;
    private Button stationSearch;
    private RecyclerView stationList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        getWindow().setTitle(getString(R.string.bus_station));
        setContentView(R.layout.activity_station_list);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        stationName = (EditText)findViewById(R.id.station_name);
        stationSearch = (Button)findViewById(R.id.station_search);
        stationList = (RecyclerView)findViewById(R.id.stationList);
        stationList.setLayoutManager(layoutManager);
        stationSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BusStation station = new BusStation(StationListActivity.this
                        , stationName.getText().toString());
                try {
                    Adapter adapter = new Adapter(StationListActivity.this, station.execute().get());
                    stationList.setAdapter(adapter);
                    System.gc();
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage());
                }
            }
        });
    }

}
