package com.gyungdal.buswait.activity.Recyclerview;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.gyungdal.buswait.R;

/**
 * Created by GyungDal on 2016-09-28.
 */

public class Holder extends RecyclerView.ViewHolder {
    public final TextView name, id, extra;
    public final CardView card;
    public final View view;

    public Holder(View itemView) {
        super(itemView);
        view = itemView;
        card = (CardView) itemView.findViewById(R.id.card);
        name = (TextView) itemView.findViewById(R.id.name);
        id = (TextView) itemView.findViewById(R.id.id);
        extra = (TextView) itemView.findViewById(R.id.extra);
    }
}
