package com.gyungdal.buswait.activity;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.WindowManager;

import com.gyungdal.buswait.R;
import com.gyungdal.buswait.activity.Recyclerview.Adapter;
import com.gyungdal.buswait.internet.BusList;
import com.gyungdal.buswait.internet.BusStation;

import java.util.concurrent.ExecutionException;

/**
 * Created by GyungDal on 2016-09-28.
 */

public class BusListActivity extends AppCompatActivity {
    private static final String TAG = BusStation.class.getName();
    private RecyclerView busList;
    private String busId;
    private Adapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
        }
        busId = getIntent().getStringExtra("id");
        getWindow().setTitle(getString(R.string.bus_station));
        setContentView(R.layout.activity_bus_list);


        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        busList = (RecyclerView)findViewById(R.id.busList);
        busList.setLayoutManager(layoutManager);
        BusList busList = new BusList(busId);
        try {
            adapter = new Adapter(busList.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR).get());
        } catch (Exception e){
            Log.e(TAG, e.getMessage());
        }
        this.busList.setAdapter(adapter);
        System.gc();
    }
}
