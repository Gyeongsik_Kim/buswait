package com.gyungdal.buswait.activity.Recyclerview;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.gyungdal.buswait.Config;
import com.gyungdal.buswait.R;
import com.gyungdal.buswait.activity.BusListActivity;
import com.gyungdal.buswait.internet.Item;

import java.util.ArrayList;

/**
 * Created by GyungDal on 2016-09-28.
 */

public class Adapter extends RecyclerView.Adapter<Holder> {
    private static final String TAG = Adapter.class.getName();
    private ArrayList<Item> datas;
    private Activity activity;

    public Adapter(Activity activity, ArrayList<Item> datas){
        this.activity = activity;
        this.datas = datas;
    }
    public Adapter(ArrayList<Item> datas){
        this.datas = datas;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_bus, parent, false);
        Holder userViewHolder = new Holder(v);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(final Holder holder, final int position) {
        if (datas.get(position).getIsSetUp()) {
            //처음에 뜰 리스트
            //TODO : 처음에 나올 리스트
        } else {
            if (datas.get(position).getIsStation()) {
                //버스 정류장일경우
                holder.card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(activity, BusListActivity.class);
                        intent.putExtra("id", datas.get(position).getId());
                        activity.startActivity(intent);
                    }
                });
                holder.id.setText(String.valueOf(position + 1));
                holder.name.setText(datas.get(position).getName()
                        + "   (" + datas.get(position).getLocate() + " 방면)");
                holder.extra.setText(getStationType(datas.get(position).getExtra()));
            } else {//버스정류장이 아닌경우
                if(datas.get(position).getName().contains("("))
                    holder.id.setText(datas.get(position).getName()
                        .substring(0, datas.get(position).getName().indexOf('(')).trim());
                else
                    holder.id.setText(datas.get(position).getName().trim());

                String name;
                if (datas.get(position).getLocate().contains("(")) {
                    name = datas.get(position).getName() + "   "
                            + datas.get(position).getLocate();
                } else {
                    name = datas.get(position).getName()
                            + "   (" + datas.get(position).getLocate() + ")";
                }
                holder.name.setText(name);
                if (datas.get(position).getCanRealTimeUpdate()) {
                    holder.extra.setText("도착 : " + Integer.valueOf(datas.get(position).getExtra()) / 60 + "분, " +
                            Integer.valueOf(datas.get(position).getExtra()) % 60 + "초 전");
                    holder.card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(holder.view.getContext(),
                                    "설정 실패 : 구현중...", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    //실시간으로 정보 받기가 불가능한 경우
                    holder.extra.setText("실시간으로 정보 받기가 불가능 합니다.");
                    holder.card.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Toast.makeText(holder.view.getContext(),
                                    "설정 실패 : 실시간 정보 확인 불가능", Toast.LENGTH_SHORT).show();
                        }
                    });

                }
                if(holder.id.getText().length() > 3)
                    holder.id.setTextSize(25);
            }
        }
    }

    private String getStationType(String types){
        Log.i(TAG, types);
        String result = "";
        while(types.length() > 2){
            if(!result.isEmpty())
                result += ", ";
            if(types.contains("|")) {
                result += getBusString(Integer.valueOf(types.substring(0, types.indexOf('|'))));
                types = types.substring(types.indexOf('|') + 1, types.length());
            }
            else {
                result += getBusString(Integer.valueOf(types));
                break;
            }
        }
        return result;
    }

    private String getBusString(int type){
        switch(type){
            case Config.BUS_TYPES.TRUNK :
                return "간선";
            case Config.BUS_TYPES.BRANCH :
                return "지선";
            case Config.BUS_TYPES.GREATER :
                return "광역";
            case Config.BUS_TYPES.LOOP :
                return "순환";
            case Config.BUS_TYPES.AIRPORT :
                return "공항";
            case Config.BUS_TYPES.NORMAL :
                return "일반";
            case Config.BUS_TYPES.SEAT :
                return "좌석";
            case Config.BUS_TYPES.SUBURB :
                return "외곽";
            case Config.BUS_TYPES.TOWN :
                return "마을";
            case Config.BUS_TYPES.QUICK :
                return "급행";
            case Config.BUS_TYPES.DIRECT :
                return "직행";
            case Config.BUS_TYPES.OUTER :
                return "시외";
            case Config.BUS_TYPES.RURAL :
                return "농어촌";
            default :
                Log.i(TAG, "What?");
                Log.i(TAG, "" + type);
                return null;
        }
    }

    @Override
    public int getItemCount() {
        return datas != null ? datas.size() : 0;
    }

    public void refreshView(ArrayList<Item> datas){
        if(this.datas != null)
            this.datas.clear();
        this.datas = datas;
        notifyDataSetChanged();
    }
}
