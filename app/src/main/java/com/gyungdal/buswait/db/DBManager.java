package com.gyungdal.buswait.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by GyungDal on 2016-09-26.
 */

public class DBManager extends SQLiteOpenHelper {
    private static final String TAG = DBManager.class.getName();
    public DBManager(Context context, String name
            , SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE data(id integer primary key autoincrement" +
                ", station_id text, bus text);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
    void exec(String query) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(query);
        db.close();
    }
    public void insert(String query){
        exec(query);
    }

    public void delete(String query) {
        exec(query);
    }

    public ArrayList<DBItem> getData() {
        ArrayList<DBItem> result = new ArrayList<DBItem>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("select * from data", null);
        while(cursor.moveToNext()) {
            int no = cursor.getInt(0);
            String station_id = cursor.getString(1);
            String bus = cursor.getString(2);
            Log.i(TAG, "DB no : " + no);
            Log.i(TAG, "Station ID : " + station_id);
            Log.i(TAG, "Bus : " + bus);
            result.add(new DBItem(station_id, bus));
        }
        db.close();
        return result;
    }
}
