package com.gyungdal.buswait.db;

/**
 * Created by GyungDal on 2016-09-26.
 */

public class DBItem {
    public String station_id;
    public String bus;

    public DBItem(String station_id, String bus){
        this.station_id = station_id;
        this.bus = bus;
    }
}
